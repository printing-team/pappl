pappl (1.3.1-2) unstable; urgency=medium

  * debian/control: add dependency on libavahi-client-dev
                    (new version on pkg-config also validates whole
                     dependency tree)
    (Closes: #1023926)

 -- Thorsten Alteholz <debian@alteholz.de>  Sat, 25 Feb 2023 11:44:24 +0100

pappl (1.3.1-1) unstable; urgency=medium

  * Update to new upstream version 1.3.1.
  * update symbols file

 -- Thorsten Alteholz <debian@alteholz.de>  Sat, 04 Feb 2023 12:44:24 +0100

pappl (1.2.1-1) unstable; urgency=medium

  * Update to new upstream version 1.2.1.
  * debian/tests/control: add Restrictions: allow-stderr
    (Closes: #1011990)
    (thanks to elbrus for the suggestion)

 -- Thorsten Alteholz <debian@alteholz.de>  Sun, 19 Jun 2022 11:35:04 +0200

pappl (1.2.0-1) unstable; urgency=medium

  * Update to new upstream version 1.2.0.
  * update symbols file
  * install more header files in -dev package

 -- Thorsten Alteholz <debian@alteholz.de>  Sun, 15 May 2022 23:53:17 +0200

pappl (1.1.0-3) unstable; urgency=medium

  * debian/tests: fix testsuite

 -- Thorsten Alteholz <debian@alteholz.de>  Mon, 17 Jan 2022 18:02:23 +0100

pappl (1.1.0-2) unstable; urgency=medium

  * upload to unstable

 -- Thorsten Alteholz <debian@alteholz.de>  Sun, 16 Jan 2022 08:05:23 +0000

pappl (1.1.0-1) experimental; urgency=medium

  * Update to new upstream version 1.1.0.
  * upload to experimental
  * debian/control: add myself to Uploaders:
  * do not use network in testsuite
  * due to upstream fix override for libpappl-dev is no longer needed
  * update symbols file

 -- Thorsten Alteholz <debian@alteholz.de>  Sat, 15 Jan 2022 13:05:23 +0000

pappl (1.0.3-2) unstable; urgency=medium

  * Migrate to unstable
  * Remove myself from Uploaders
  * S-V: Update to 4.6.0 without changes needed
  * Update Salsa CI configuration

 -- Didier Raboud <odyx@debian.org>  Fri, 03 Sep 2021 17:04:22 +0200

pappl (1.0.3-1) experimental; urgency=medium

  * New 1.0.3 upstream release
    - Drop backported patches

 -- Didier Raboud <odyx@debian.org>  Fri, 23 Apr 2021 10:55:26 +0200

pappl (1.0.2-2) experimental; urgency=medium

  * Backport upstream patch to fix arm FTBFS;
    - Fix some more threading issues (Issue #155, Issue #162)

 -- Didier Raboud <odyx@debian.org>  Wed, 14 Apr 2021 08:09:47 +0200

pappl (1.0.2-1) experimental; urgency=medium

  * New 1.0.2 upstream release
    - Backport upstream fix for test suite
    - Handle when the Avahi daemon is not running (Issue #129)

 -- Didier Raboud <odyx@debian.org>  Sun, 11 Apr 2021 19:09:06 +0200

pappl (1.0.1-2) unstable; urgency=medium

  * Set upstream metadata fields: Security-Contact

 -- Didier Raboud <odyx@debian.org>  Wed, 03 Feb 2021 22:47:35 +0100

pappl (1.0.1-1) unstable; urgency=medium

  * Initial release. (Closes: #980200)

 -- Didier Raboud <odyx@debian.org>  Wed, 20 Jan 2021 10:20:40 +0100
